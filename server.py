import tornado.httpserver, tornado.ioloop, tornado.web
import os, subprocess, json
from gltflib import GLTF

LOCAL_DIR = os.path.dirname(os.path.abspath(__file__))
os.chdir(LOCAL_DIR)

PORT = "8888"

class COLORS:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class HTTP_STATUS:
    OK = 200
    NO_CONTENT = 204
    BAD_REQUEST = 400
    NOT_FOUND = 404
    SERVER_ERROR = 500

class CorsRequestHandler(tornado.web.RequestHandler):

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "Content-Type, origin, x-requested-with, x-compress")

    def options(self):
        self.set_status(HTTP_STATUS.NO_CONTENT)
        self.finish()

class StaticHandler(tornado.web.StaticFileHandler):

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "Content-Type, origin, x-requested-with, x-compress")
        self.set_header("Access-Control-Allow-Methods", "GET, OPTIONS")

    def options(self, kwargs):
        self.set_status(HTTP_STATUS.NO_CONTENT)
        self.finish()

    def parse_url_path(self, url_path):
        return url_path

def make_app():
    return tornado.web.Application([
        (r"/(.*)", tornado.web.StaticFileHandler, {"path": LOCAL_DIR, "default_filename": "index.html"})
        ])

if __name__ == "__main__":

    app = make_app()

    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(PORT)

    print("Server is running")

    tornado.ioloop.IOLoop.current().start()
